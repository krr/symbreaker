\documentclass[conference]{IEEEtran}

\usepackage{amsmath,amsfonts,amssymb,amsthm,textcomp}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem*{cor}{Corollary}

\theoremstyle{definition}
\newtheorem{defn}{Definition}[section]
\newtheorem{conj}{Conjecture}[section]
\newtheorem{exmp}{Example}[section]

\theoremstyle{remark}
\newtheorem*{rem}{Remark}
\newtheorem*{note}{Note}

\begin{document}
	
% paper title
\title{ShatterGlucose and BreakIDGlucose}

% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
\author{
\IEEEauthorblockN{Jo Devriendt}
\IEEEauthorblockA{University of Leuven\\ Leuven, Belgium}
\and
\IEEEauthorblockN{Bart Bogaerts}
\IEEEauthorblockA{University of Leuven\\ Leuven, Belgium}
}

\maketitle

\begin{abstract}
The defining characteristic of our submitted SAT solvers is the addition of a preprocessing step in which symmetry breaking clauses are added to the CNF theory.
\end{abstract}

\section{Introduction}
Many real-world problems exhibit symmetry, but the SAT competition and SAT challenge seldomly feature solvers who are able to exploit these symmetry properties. This discrepancy can only be explained by the assumption that for most of the problems in these competitions, symmetry exploitation is not worth the incurred overhead. To experimentally verify this hypothesis, we submit two symmetry breaking SAT solvers which in a preprocessing step detect symmetry and add symmetry breaking clauses to the CNF theory. Another aim is to simply compare both of these symmetry breaking approaches with eachother.

\section{Main Techniques}
\subsection{ShatterGlucose}
This solver represents the current state-of-the-art in symmetry detection and breaking for SAT problems. It couples Glucose \cite{glucose}, a former SAT competition winner, with Saucy \cite{saucy}, a symmetry detection tool, and Shatter \cite{shatter}, a symmetry breaking preprocessor for CNF theories. Since Saucy can not handle duplicate clauses in a CNF theory, we also initially run cnfdedup, which removes duplicate clauses from a CNF theory.

The ShatterGlucose workflow is as follows:
\begin{enumerate}
\item cnfdedup removes duplicate clauses from the CNF theory
\item Saucy 3.0 detects symmetry by converting the CNF theory to a graph
\item Using the default literal ordering, for each generator returned by Saucy, Shatter adds symmetry breaking clauses to the CNF theory
\item Glucose 2.2 solves the resulting CNF theory
\end{enumerate}
Note that we run four preprocessors before actually starting the SAT solver search.: a first one to remove duplicate clauses, a second one to detect symmetry, a third one to break symmetry, and finally Glucose uses Minisat's builtin theory simplification algorithms before starting the search.

\subsection{BreakIDGlucose}
It can be shown that the generators returned by Saucy, and the default ordering used by Shatter, are suboptimal, in the sense that in certain cases, when using other generators for the same symmetry group and another literal ordering to construct symmetry breaking clauses, the resulting search space can be exponentially smaller. BreakGlucose tries to improve these points:
\begin{enumerate}
\item cnfdedup removes duplicate clauses from the CNF theory
\item Saucy 3.0 detects symmetry by converting the CNF theory to a graph
\item BreakID searches for special symmetry subgroups and for symmetry generators of the detected symmetry group which will result in short symmetry breaking constraints
\item BreakID constructs an order adjusted to the characteristics of the detected generators and possible subgroups
\item Symmetry breaking clauses are added to the CNF theory
\item Glucose 2.2 solves the resulting CNF theory
\end{enumerate}
Note that we again run four preprocessors before actually starting Glucose's search.

\section{Main Parameters}
The main user-controlled parameters control when symmetry detection should be halted, and how much time can be devoted to looking for good symmetry generators. The submitted version of BreakIDGlucose put Saucy at a timeout of 200 seconds, while BreakID is allowed to construct symmetry generators for 200 seconds, or until 100.000 generators are constructed. Internally, BreakID also cuts of the size of any symmetry breaking formula constructed for one symmetry generator to 100 tseitin literals to reduce the overhead of introducing many tseitin literals.\\
ShatterGlucose uses no symmetry detection time limit.

\section{Special Algorithms, Data Structures, and Other Features}
We will focus on the algorithms used by BreakID, since cnfdedup has a trivial algorithm, and the other preprocessors and solver of this submission are already documented fully in literature. Some terms we will use in the next part are:

\begin{defn}
 A literal $l$ \emph{occurs} in a symmetry $S$ if $S(l)\neq l$.
\end{defn}
\begin{defn}
 The \emph{support} of a symmetry $S$ is the number of literals that occur in $S$.
\end{defn}

Given a symmetry group $\Sigma$ represented by a (small) set of generators $\sigma$, BreakID works in three steps.\\
Firstly, it constructs a large set of small generators $\gamma$ by recursively applying $\sigma$ to any recently added members of $\gamma$. If a resulting symmetry $S$ has a support smaller than or equal to a floating limit $n$, $S$ is added to $\gamma$, and $n$ is decreased to the support of $S$. If no more symmetries can be added to $\gamma$, $n$ is increased to infinity and $\sigma$ is applied to the whole of $\gamma$. This procedure continues until $\gamma$ contains all symmetries of $\Sigma$, until a timeout is reached, or until a limit to the size of $\gamma$ is reached.\\
Given this set of symmetry generators $\gamma$, the second step consists of detecting easily-breakable symmetry subgroups generated by a subset of $\gamma$. These symmetry subgroups are known in literature, and can informally be described as pigeonhole-like symmetries, which refers to the prototypical pigeonhole problem. With the right ordering of literals to construct symmetry breaking clauses, it has been shown that breaking these groups results in an exponential speedup of the solver at hand.\\
Finally, given a set of pigeonhole-like symmetry groups $\Pi$ and a set of generators $\gamma$, BreakID then creates an ordering $O$ of the literals by firstly ordering all literals occurring in $\Pi$ so that each member of $\Pi$ is completely broken. Secondly, all literals not occurring in $\Pi$ are smaller than those occurring in $\Pi$, and are subsequently ordered based on their total occurrence in $\gamma$: the lower the smaller. Given ordering $O$, symmetry breaking formula's are added to the CNF theory for any pigeonhole-like symmetry group in $\Pi$, and for any symmetry generator $S$ in $\gamma$ such that the symmetry breaking formula of $S$ contains at least one relatively short constraint. This last condition, together with the fact that we never add symmetry breaking formula's longer than 100 tseitin variables, limits the total number of clauses and tseitin variables induced by static symmetry breaking.

\section{Implementation details}
BreakID and cnfdedup were written from scratch in C++. We refer to the webpages of the other programs for their implementation details.

\section{SAT Competition 2013 Specifics}
ShatterGlucose and BreakIDGlucose were both submitted to the application and hard-combinatorial SAT+UNSAT tracks of the SAT13 competition. GCC 4.8.0 was used by the organizers, with -O3 optimization flags. The resulting binaries were 64 bit.

\section{Availability}
An older version of Shatter and Saucy are publicly available at http://www.aloul.net/Tools/shatter/, where version 3.0 is available upon request. Glucose 2.2 is available at https://www.lri.fr/\texttildelow simon/?page=glucose. BreakID and cnfdedup are available at https://bitbucket.org/krr/symbreaker.

\section{Acknowledgements}
We would like to thank 
\begin{enumerate}
 \item Paul T. Darga, Mark Liffiton and Hadi Katebi for the symmetry detection tool Saucy
 \item Fadi A. Aloul and Paul T. Darga for the symmetry breaking tool Shatter
 \item Laurent Simon and Gilles Audemard for their SAT solver Glucose
\end{enumerate}

\bibliographystyle{IEEEtran}
\bibliography{krrlib}

\end{document}
