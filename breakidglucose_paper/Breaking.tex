%\bart{wat indien er slechts weinig tijd gegeven is?}
%\bart{mogelijks goed idee: een ``structural symmetry'' is ``a symmetry induced by a permutation of literals''... Deifnieren in secite 2 en dan moeten we niet steeds induced herhalen}

%\bart{in deze secite: duizend keer ``row or column''. Ik zou in het begin zeggen: we focussen op ``row'' maar alles is ook geldig voor ``column''}

%\bart{nog een algemene opmerking: dingen als ``Given a SAT problem $\Pi^{enc}= ...$'', dat mag je niet doen. TENZIJ je ook $\Pi$ nodig hebt. Maar anders is het gewoon ``Given a SAT problem $\Pi=...$, het maakt immers niet uit of dat de encoding van iets hoger niveau is of niet.}

\label{breaking}

In the previous section, we showed that SAT encodings of (piecewise) variable or value interchangeable CSPs are respectively (piecewise) row or column interchangeable. Since a CNF theory has no inherent rows or columns, we will ignore column interchangeability for the rest of this paper, as it is merely another way of looking at row interchangeability. Note that we also are not investigating symmetry groups arising from both row \emph{and} column interchangeable variable matrices, which is a related but different topic.

Now, given that a row interchangeability symmetry group can be broken very efficiently, one should strive to exploit row interchangeability in SAT theories. Two obstacles currently block our path. The first is that it is unknown how one can efficiently infer from a CNF theory that a significant subset of variables can be structured as a row interchangeable variable matrix.
This is contrasts with higher level CP languages such as MiniZinc~\cite{MiniZinc}, or relational languages such as \fodot~\cite{WarrenBook/DeCatBBD14} and Alloy~\cite{tosem/Jackson02}, where symmetry groups that reduce to row interchangeability in a lower level CNF theory are easily detectable~\footnote{The original context that led to this research was symmetry exploitation for the \idp system~\cite{WarrenBook/DeCatBBD14}. As pointed out in \cite{dam/shlyakter2007}, when searching for a relation $\mathcal{R}:D_1 \times \ldots \times D_n$, it is possible to break the symmetry induced by the permutation group of a single disjoint domain $D_i$ with a polynomially sized symmetry breaking constraint. This is consistent with our findings, where the reduction of domain independent relational constraints to SAT induces row interchangeability in the resulting CNF theory.}.
Secondly, even if a set of symmetry inducing permutations swapping consecutive rows of a variable matrix is given, current SAT symmetry breaking mechanisms would use this knowledge in an incomplete way: they might post incomplete symmetry breaking constraints for the row interchangeability symmetry group.

We continue this section by summarizing how symmetry detection and static symmetry breaking are currently implemented in SAT.
Next, we explain the shortcomings of this approach with respect to interchangeable rows. 
Finally, we propose a complete row interchangeability breaking approach for SAT, and give a first attempt at row interchangeability detection for SAT.

%\jo{
%It is worth stressing that we only address symmetry groups arising from row interchangeable variable matrices of SAT problems, and not variable matrices that are both row \emph{and} column interchangeable. As mentioned in Section \ref{sec:sym_breaking_constr}, it is unknown whether efficient symmetry breaking constraints exist for the symmetry groups induced by the latter.
%}

\subsection{Breaking Row Symmetry in SAT}
\label{breaking_sym}
In SAT, symmetry is often detected by converting a SAT problem $\Pi$ to a graph $G$ with the property that automorphisms of $G$ correspond to permutations of literals of $\Pi$ that induce a symmetry  of $\Pi$.
Graph automorphism detection tools~\cite{saucy,bliss} are used to detect generators of the graph automorphism group, 
which then correspond to generators of a symmetry inducing permutation group of $\Pi$. 
The detected symmetry can be broken statically, e.g., by Shatter \cite{Shatter}, or dynamically, e.g., by Symmetry Propagation \cite{ictai/DevriendtBMDD12}, two methods that are designed to break any symmetry induced by a permutation of literals. 
Many other approaches exist, of which we briefly mention SymChaff~\cite{symchaff}. SymChaff expects so-called \emph{$k$-complete multiclass symmetry} specifications to be given as part of its input, and can break these symmetries completely dynamically. We suspect that $k$-complete multiclass symmetry groups actually are row interchangeability symmetry groups, but we are not able to confirm this link yet.

The most convenient of these symmetry breaking approaches is the static symmetry breaking approach taken by Shatter, a preprocessor for SAT solvers that works on any CNF
theory. It uses Saucy~\cite{saucy} for symmetry detection and extends
the CNF theory with symmetry breaking constraints. Below, we explain this approach in detail and argue that it fails to exploit row interchangeability. \\

Given a SAT problem $\Pi=(W,T)$, Saucy outputs a set of permutations of literals of $\Pi$ such that each of these permutations induce a symmetry of $\Pi$. 
From now on, we denote this induced set of symmetries by $\Sigma$, which is a \emph{minimal} set of generators of a symmetry group $G_\Sigma$ of $\Pi$. 
For this section, we assume that these symmetries are induced by permutations of \emph{variables} only, since these induce variable symmetries, which we are interested in . 
This is neither a very restrictive nor an essential assumption.

%$\old{Now, given a total order $\lew$ on $W$,  Shatter follows  \cite{crawford1996symmetry} by defining a total lexicographic order $\lex$ on the assignment space where $\alpha_1\lex\alpha_2$ if for some $w$, $\alpha_1(w)=\lfalse$ and $\alpha_2(w)=\ltrue$ while for every $w'\lew w$, $\alpha_1(w')=\alpha_2(w')$.
%Shatter proceeds by generating a symmetry breaking constraint $\phi_\sigma=\alpha \lexeq \sigma(\alpha)$ for each $\sigma \in \Sigma$.}

Following \cite{crawford1996symmetry}, Shatter uses a total order $\lew$ on the Boolean
  variables in $W$ to induce a total lexicographic order $\lex$ on the
  assignment space: $\alpha_1\lex\alpha_2$ if for some $w$,
  $\alpha_1(w)=\lfalse$ and $\alpha_2(w)=\ltrue$ while for every
  $w'\lew w$, $\alpha_1(w')=\alpha_2(w')$. Shatter then extends the
  CNF theory with a symmetry breaking constraint $\phi_\sigma \equiv \alpha
  \lexeq \sigma(\alpha)$ for each $\sigma \in \Sigma$.
Together, these constraints cut away all assignments that are lexicographically larger than their image under the symmetries in $\Sigma$, 
but not necessarily under all symmetries in the group $G_\Sigma$.

This approach still has one free parameter, namely the ordering $\lew$. Since each variable $w$ typically has an integer identifier $id(w)$, Shatter pragmatically determines $w \lew w'$ iff $id(w)<id(w')$.
However, as we shall see in Example \ref{small_ph}, the ordering is actually very important and choosing the correct ordering can make the difference between breaking the entire symmetry group and breaking almost nothing.


%\begin{equation}
%\label{shatter_constraint}
%\forall w_1: (\forall w_2 \lew w_1: w_2{=_b} P(w_2)) \Rightarrow w_1 {\leq_b} P(w_1)
%\end{equation}\bart{deze zin is geen CNF. Leg uit: instantieren van variabelen of schrijf iets LP style}
%\bart{wat volgt is niet zo vlot leesbaar. Ik snap het omdat ik shatter ken, maar anders zou ik hier moeilijk doorgeraken} 
%Note that given an assignment $\alpha$, $S_P(\alpha)(w)=\alpha(P(w))$. So constraint \ref{shatter_constraint} states that for a certain assignment $\alpha$, the first variable that takes a value different from its value in the symmetrical assignment $S_P(\alpha)$ must be assigned the smaller value. Only the lexicographically smaller assignment will satisfy this constraint, removing the larger symmetrical constraint from the set of solutions.
%Since SAT-solvers only allow constraints in clausal form, Shatter akes as order on the values $\ltrue {\leq_b} \lfalse$, and propositionalizes constraint \ref{shatter_constraint} to:
%\begin{equation}
%\label{shatter_constraint_prop}
%\forall w_1: (\forall w_2 \lew w_1: w_2 \Leftrightarrow P(w_2)) \Rightarrow (w_1 \Rightarrow P(w_1))
%\end{equation} 
%As order on the variables, Shatter simply takes the order on the variables' identifiers in the CNF theory\footnote{The identifier of a SAT-variable is typically simply an integer.}. Using auxiliary variables, Shatter transforms constraint \ref{shatter_constraint_prop} to a set of clauses with cardinality linear in the number of variables. However, this only adds symmetry breaking constraints for one symmetry - $S_P$. Since the number of permutations in a permutation group can be exponential, adding constraint \ref{shatter_constraint} for all induced symmetries is not an option. Shatter therefor resorts to only adding the symmetry breaking constraint \ref{shatter_constraint_prop} for each symmetry induced by a generator in a minimal generator set for the symmetry inducing permutation group.

%Given the above modus operandi of Shatter, Shatter is a sound symmetry breaking approach, but it does not completely break symmetry for all row interchangeable CSPs. We illustrate this the following pigeonhole example:
\begin{example}
\label{small_ph}
Let $PH_{32}=(W,T)$ be a standard encoding of a pigeonhole instance with 3 pigeons and 2 holes, where $W$ consists of variables $w_{ij}, i\in \{1,2,3\},j\in\{1,2\}$ and variable $w_{ij}$ denotes whether pigeon $i$ is assigned hole $j$. 
The mapping $M:(i,j)\mapsto w_{ij}$ is a variable matrix for $PH_{32}$ where every row of $M$ represents a pigeon, and every column a hole. 

Now, we focus on the interchangeability of pigeons, or equivalently, on the $M$-row interchangeability.
Suppose that Saucy detects that all pigeons are interchangeable, and returns a minimal set of generator permutations that induce the interchangeable pigeon symmetry group. For example, Saucy returns $P_{12}$ and $P_{13}$, where $P_{ij}$ is the permutation that swaps pigeon $i$ with pigeon $j$, or equivalently, the permutation that swaps row $i$ with row $j$ in $M$.

Suppose the order on the variables $\lew$ is defined as follows:
\begin{equation}
\label{order_ph}
w_{11} \lew w_{12} \lew w_{21} \lew w_{22} \lew w_{31} \lew w_{32}.
\end{equation}
Shatter then posts constraints that intuitively read as
``pigeon 1 resides in a hole smaller than or equal to the hole in which pigeon 2 resides'' and ``pigeon 1 resides in a hole smaller than or equal to the hole in which pigeon 3 resides''.
It is clear that these constraints do not break all symmetries, because for example, they do not relate pigeon 2 and pigeon 3. 
This is a suboptimal outcome, since we already mentioned in Proposition \ref{row_sym_can_be_broken_completely} that there exists a short symmetry breaking constraint that completely breaks a row interchangeability symmetry group. This constraint is quite simple: just state that all rows must be lexicographically ordered. In other words, the constraints
``pigeon 1 resides in a hole smaller than or equal to the hole in which pigeon 2 resides'' and ``pigeon 2 resides in a hole smaller than or equal to the hole in which pigeon 3 resides'' 
would break the $M$-row interchangeability symmetry group completely. 
This is simply due to the transitivity of the ``smaller than or equal to'' relation, which implies that also pigeon 1's hole is smaller than or equal to pigeon 3's hole.

Now we see the importance of the variable ordering: 
had Shatter used the variable ordering
\begin{equation}
\label{order_ph_good}
w_{21} \lew w_{22} \lew w_{11} \lew w_{12} \lew  w_{31} \lew w_{32},
\end{equation}
its constraints would have completely broken the $M$-row interchangeability symmetry group.
% The constraints posted by Shatter will then become 
% \begin{align*}
% \label{shatter_constraints}
% \alpha \lexeq S_{P12}(\alpha)\\
% \alpha \lexeq S_{P13}(\alpha)\\
% \alpha \lexeq S_{Q12}(\alpha)
% \end{align*}
\end{example}
% The question then becomes: do the constraints from Example \ref{small_ph} completely break $G_{row}$? The answer is negative, since the assignments 
% \[ \begin{array}{ccc}
% \alpha_1=\{& w_{11}=\lfalse; & w_{12}=\ltrue; \\
% & w_{21}=\ltrue;  & w_{22}=\lfalse; \\
% & w_{31}=\lfalse; & w_{32}=\ltrue \}
% \end{array} \]
% and
% \[ \begin{array}{ccc}
% \alpha_2=\{& w_{11}=\lfalse; & w_{12}=\ltrue; \\
% & w_{21}=\lfalse; & w_{22}=\ltrue; \\
% & w_{31}=\ltrue;  & w_{32}=\lfalse \}
% \end{array} \]
% are both lexicographically smaller than $S_P(\alpha_1)$ resp. $S_P(\alpha_2)$ for each $P \in \theta$, but nonetheless belong to the same symmetry class, since $\alpha_1=(S_{P12}\circ S_{P13}\circ S_{P12})(\alpha_2)$.
% \jo{meer uitleg nodig over wanneer iets lexicographically smaller is?}
% 
% This is a suboptimal outcome, since we already mentioned in Proposition \ref{row_sym_can_be_broken_completely} that there exists a short symmetry breaking constraint that completely breaks a row-interchangeability symmetry group such as $G_{row}$. This constraint is quite simple: just state that all rows must be lexicographically ordered \cite{row_column_sym_csp}. In other words, the constraints
% \begin{align*}
% %\label{complete_constraints}
% \alpha \lexeq S_{P12}(\alpha)\\
% \alpha \lexeq S_{P23}(\alpha)
% \end{align*}
% would suffice to break $G_{row}$ for $PH_{SAT/3,2}$ completely, since they state that the first and second row must be ordered lexicographically, and the second and third, which by . It is clear that to allow only assignments with the rows ordered according to the variable matrix, it is sufficient to post the constraint $\alpha \lexeq S_{Pi,i+1}(\alpha)$ for each row $i<max_i$.

These observations for the pigeon hole problem can be  generalized:
\begin{proposition}
\label{complete_sat_constraints}
Given a SAT problem $\Pi=(W,T)$ with variable matrix $M:Ro\times Co\to W$ such that  $\Pi$ is $M$-row interchangeable.
Assume without loss of generality that $Ro=\{1,\dots,n\}$ and $Co=\{1,\dots m\}$.
% Without loss of generality we can assume an order $r_i<r_{i+1}$ on the row indices $Ro=\{r_1,\ldots,r_n\}$ and an order $c_i<c_{i+1}$ on the column indices $Co=\{c_1,\ldots,c_m\}$. 
Let $\lew$ be the total order defined by $w_{ij} \lew w_{kl}$ if $i<k$ or if $i=k$ and $j<l$ and
let $\sigma_{i,j}$ denote the symmetry induced by swapping row $i$ and $j$.
Then, the set of constraints 
\[
	\alpha \lexeq \sigma_{i,i+1}(\alpha) \text{ for } 0<i<n
\]
breaks the $M$-row interchangeability symmetry group completely.
\end{proposition}
\begin{proof}
Applying Shatter's equivalency preserving compression techniques for symmetry breaking constraints turns $\alpha \lexeq \sigma_{i,i+1}(\alpha)$ into a small constraint stating that the assignment to the variables of row $i$ is lexicographically smaller than or equal to the assignment to the variables of row $i+1$. Hence, Proposition \ref{complete_sat_constraints} is in fact a reformulation of the constraints used to break row interchangeability completely in CSPs~\cite{dam/shlyakter2007,row_column_sym_csp}.
\end{proof}
Proposition \ref{complete_sat_constraints} shows that if the order on the variables correctly follows the matrix structure \emph{and} the set of symmetry inducing generators consists of the permutations that swap two consecutive rows, then the symmetry breaking constraints posted by Shatter are complete. 
Hence, a good way to improve Shatter is to ensure that the chosen variable ordering and the set of symmetry generators are compatible. 
As we explain later, this is the approach we took with BreakID.

% Note that the total ordering on the variables $\lew$ must correspond to the order on both the row- and column-indices to guarantee total $Enc$-row ordering! For example, if our constraints would read
% \begin{align*}
% %\label{complete_constraints}
% \alpha \lexeq S_{P12}(\alpha)\\
% \alpha \lexeq S_{P23}(\alpha)
% \end{align*}
% but 
% \begin{equation}
% \label{bad_order_ph}
% w_{21} \lew w_{22} \lew w_{11} \lew w_{12} \lew w_{31} \lew w_{32}
% \end{equation}
% Then row $1$ and $3$ would not be ordered.

\subsection{Detecting Row Symmetry in SAT}
\label{detecting_sym}
We remark again that detecting a set of symmetry inducing permutations that swap two consecutive rows is not a trivial task, simply because a CNF theory has no builtin notion of rows or matrices. A good symmetry detection tool however should be able to extract the row interchangeable matrix structure of (a subset of) variables, if such structure is present. Currently, Saucy is not ``row interchangeability aware'', and hence getting the right symmetry inducing generators is not guaranteed; the only guarantee Saucy gives is that the set of generator permutations is minimal.
For instance, in Example \ref{small_ph}, Saucy could also have returned the set $\{P_{123},P_{13}\}$, which still induces the row interchangeability symmetry group, but which reveals less of the matrix structure of $PH_{32}$.
We give a first attempt to remedy this symmetry detection problem in this section. \\

%So far we avoided the question of how to find a variable matrix and detect row interchangeability for that matrix. In many high level languages such as MiniZinc \cite{MiniZinc} or \fodot \cite{WarrenBook/DeCatBBD14}, symmetry inducing row permutations are easily detectable \cite{compositional_sym_detection}, since the matrix organisation of variables or the interchangeability of objects is clearly visible. In SAT however, detecting row interchangeability turned out to be a difficult problem, since the interchangeability properties of high level language constructs is obfuscated during the transformation to SAT. 
%This comes from the fact that many practical SAT theories are in effect an encoding of some higher level problem and that detecting these symmetries on that higher level is often an easy task.
%For example, approaches such as MiniZinc \cite{conf/cp/NethercoteSBBDT07} that encode CP specifications as SAT problems could benefit from symmetry detection algorithms in CP \todo{cite cite cite}, for example using Proposition or THeorem \todo{ref}, interchangeable variables in a CSP  correspond to interchangeable rows in the standard encoding. 
%Similar arguments hold for example for first-order theories that are grounded to SAT: interchangeable domain elements on the FO level also corresponds to interchangeable rows in in the grounding. 
%Using these insights, a general take-away advice for people that use SAT solvers to solve an encoding of their problem is, that it might be a good idea to detect symmetry on the high level because converting a high level problem specification to CNF drastically reduce the structural information of the problem
%These insights are used for example in the \idp system \cite{WarrenBook/DeCatBBD14}, which grounds first-order theories to SAT, but first detects symmetries on the FO level, where they are obvious to find.

%As argued, it is good practice to detect symmetries on the higher level before encoding the specification of a SAT problem. 
%However, this is not reality, many problems are still solved by encoding them to SAT (thus, losing all structural information) and calling an out-of-the-box SAT solver. 
%Furthermore, SAT solver development highly focusses on this approach, as can be seen by the instances in the SAT competitions where no format is defined for adding additional structural information to the CNF.
\begin{comment}
In order to break row interchangeability completely, per Proposition \ref{complete_sat_constraints},  we need
\begin{itemize}
	\item a variable matrix $M$,
	\item an order on the variables that is compatible with $M$
	\item a set of $M$-row symmetries that represent the swapping of all consecutive rows of $M$
\end{itemize}
\end{comment}
Taking into account that we want to detect multiple row interchangeability symmetry groups, we formalise the symmetry detection problem as follows:
\begin{problem}[Piecewise Row Interchangeability Detection for CNF (PRID)]
\label{RIDP}
Given a SAT problem $\Pi=(W,T)$, \emph{PRID} consists of finding a partition $\{W_1,\dots,W_n\}$ of $W$ and  a set $\{M_1,\dots, M_n\}$ such that 
\begin{itemize}
	\item every $M_i$ is a variable matrix of $W_i$,
	\item $\Pi$ is $M_i$-row interchangeable for every $i$,
	\item the sum of the number of rows of each $M_i$ is maximal.
\end{itemize}
 \end{problem}
The third condition ensures that the detected symmetry groups are significant, and avoids a trivial solution to the PRID problem by partitioning $W$ into singleton sets.

To the best of our knowledge, the PRID problem has not been mentioned in literature, let alone solved. 
In the rest of this subsection, we design a first attempt at an algorithm for PRID. 
We give a rough sketch. \\

% First, we introduce some more terminology. For two permutations $P_1,P_2$, we define the set of shared variables as $Sha_{P1P2}=\{w \in W \mid P_1(w)\neq w \land P_2(w) \neq w\}$. Also, an \emph{involution} is a permutation $P$ which is its own inverse: $P=P^{-1}$.

First, remark that in Example \ref{small_ph} we assumed Saucy returned a set $\Sigma$ of generator permutations that already contained some structure: the generator permutations were row swaps of the known variable matrix. 
Permutations that swap rows carry a partial structure of their corresponding variable matrix in them, so it makes sense to investigate those closer. A first characterization of a row swapping permutation $P$ is that $P$ is an \emph{involution}: $P=P^{-1}$.
So if we have a set of symmetry inducing involutions, they might encode row swapping permutations on some unknown variable matrix. This leads to the following terminology:

Given an involution $P$, a \emph{candidate row} of $P$ is a sequence of variables $c=w_1,\dots,w_n$, such that
\begin{itemize}
	\item variable $w$ does not occur in $c$ if $P(w)=w$,
	\item if $P(w) \neq w$, then either $w$ occurs in $c$ or $P(w)$ occurs in $c$ (but not both).
\end{itemize}
Given a candidate row $c= w_1, \ldots, w_n$ of $P$, its \emph{symmetrical counterpart} is the candidate row $P(c)=P(w_1),\dots,P(w_n)$. Since $P$ is an involution, $P(P(c))=c$, which partitions all candidate rows of $P$ into pairs of symmetrical counterparts $\{c,P(c)\}$. 
Note that if $P$ is a symmetry inducing involution for a problem $\Pi$, then each such pair of symmetrical counterparts actually represents a row-interchangeable variable matrix with two rows for $\Pi$.

Now, let $\Sigma$ be a set of generator permutations detected by Saucy, and let $P_1,P_2 \in \Sigma$ be two symmetry inducing involutions. If $P_1$ and $P_2$ share a common candidate row $c$, but do not share any other variables, then $M=\{P_1(c), c, P_2(c)\}$ forms a set of three interchangeable rows. And if $M$ contained a candidate row $c_2$ of some other involution $P_3 \in \Sigma$, but $M$ does not contain any variables from the symmetrical counterpart $P_3(c_2)$, then $M$ can be extended with $P_3(c_2)$ as fourth row.

These observations lead to the following algorithm sketch:

Initially, the algorithm searches for two involutions  $P_1,P_2 \in \Sigma$ that share a candidate row and that do not share any other variables. These form a variable matrix $M_i$ with three rows. Then the algorithm searches iteratively for more involutions which share a row with $M_i$, but no other variables. If one is found, $M_i$ is extended with another row. If no more can be found, $M_i$ is added to the set of detected variable matrices, and all permutations $Q$ that share a variable with $M$ are removed from $\Sigma$. Then the search restarts by looking for two new $P_1',P_2' \in \Sigma$ to form a new variable matrix.

By removing all permutations $Q$ that share a variable with a variable matrix, we guarantee that the detected variable matrices share no variables, and hence the above algorithm outline finds a variable matrix partition as required by the PRID problem.

Recall that Saucy does not guarantee that any involutions are present in its generator set $\Sigma$. However, the symmetry inducing permutation group $G_\Sigma$ generated by $\Sigma$ is guaranteed to contain any symmetry inducing row swapping permutation.
Hence,  before we perform the above described row-interchangeable matrix extraction, we try to find more involutions contained in $G_\Sigma$. This is done by a heuristic enumeration scheme: given the generator set $\Sigma$ outputted by Saucy, we take two elements $\sigma, \sigma' \in \Sigma$. If $\sigma \circ \sigma'$ or $\sigma' \circ \sigma$ permutes relatively few literals, we extend $\Sigma$ with it. This process is repeated until timeout.
Since row candidates typically are relatively small involutions, the focus on small compositions hopefully increases the number of involutions in $\Sigma$ which represent row swaps in variable matrices.

To summarize: our answer to the PRID problem in CNF consists of two steps (\textbf{i}) a heuristic method to generate a part of the permutation group generated by the output of Saucy that hopefully contains many involutions, and (\textbf{ii}) a variable matrix extraction phase based on involutions swapping two rows of a matrix. 
%\bart{Even though this is still a very ad-hoc method, the latest SAT competition showed that the performance gain obtained by this ad-hoc method is larger than all other advances of SAT solvers in the period 2012-2013 combined.}
%\bart{ik heb dat laatste geschreven, maar dat is ook weer niet waar natuurlijk. 'k wou hier gewoon nog iets hebben dat even de nadruk legt dat het misschien ad-hoc is maar wel goed en ZEKER goed als research question naar de community toe om algoritmes hbiervoor te verzinnen.Dit meot zeker ook in de conclusie EN inde intro staan dat 1 van de belangrijkste bijdrages van deze paper is: PRID stellen EN aantonen dat PRID en enorm relevant probleem is dat SAT een serieuze boost kan geven!}
% We did not prove that the extracted permutations indeed induce row-interchangeability symmetry groups, but instead gave intuitive properties of what such permutations should look like. However, we only ever use compositions of variable permutations outputted by Saucy to construct symmetry breaking constraints with, hence our approach is still sound, since each composition of two symmetry-inducing variable permutations is again symmetry-inducing.
%TODO: LET OP (BART zegt dit): bovenstaande doet lijken alsof je niet weet waar je mee bezig bent. We hebben het wel goed genoeg doordacht en beargumenteerd om te weten dat dit goeie symmetrieen zijn volgens mij.

\begin{comment}
In cycle notation:
\begin{align*}
P12 \circ P23 &= (w_{21} w_{31} w_{11})(w_{22} w_{32} w_{12})\\
P12 \circ Q12 &= (w_{11} w_{22})(w_{12} w_{21})(w_{31} w_{32})\\
Q12 \circ P23 \circ P12 &= (w_{11} w_{32} w_{21} w_{12} w_{31} w_{22})\\
\end{align*}
It is not obvious how we can derive from $\theta'$ that there exists a variable matrix $Enc$ for $PH_{SAT/3,2}$ such that the rows and columns of $Enc$ are interchangeable. 
\end{comment}

\begin{comment}

\subsection{Complete static symmetry breaking in SAT}
It is known that the symmetry of an $M$-row interchangeable CSP $\Pi$ can be broken by posting the constraint that each assignment must be \emph{$M$-row ordered} \bart{ik zou eerst de def geven voor ik zo een claim geef}  \cite{row_column_sym_csp}.
\begin{definition} \bart{deze def vereist op zijn minst een orde}
An assignment $\alpha$ for an $M$-row interchangeable CSP is \emph{$M$-row ordered} if for each two rows $i,j$, the values of the smaller row form a lexicographically smaller string than the variables of the larger row. More formal: $\forall i,j: i<j \Rightarrow \forall k: (\forall l: l<k \Rightarrow v_{il}=v_{jl}) \Rightarrow v_{ik}\leq v_{jk}$.
\bart{voor de more formal: bedoel je ``if and only if $\alpha$ satisfies ...}
\end{definition}
\begin{example}
\label{example_ordered}
Continuing Example \ref{small_ph}, $\alpha_2$ is $M$-row ordered, while $\alpha_1$ is not.
\end{example}
If we now can post such an \emph{$M$-row ordered} constraint in CNF, we will have a complete symmetry breaking approach for $M$-interchangeable row symmetry in SAT.

To construct an $M$-row ordered constraint, we only need to enforce that the variables in each two subsequent rows $i$ and $i+1$ form two lexicographically ordered strings of which the first one is the smaller\footnote{We use $x+1$ to denote $x$'s successor in a total ordering.Bart: dit bestaat niet altijd in een total order (bijvoorbeeld $\mathbb{R}$)}. The transitivity property of an order will then ensure that each assignment will be $M$-row ordered. Enforcing for each two $i,i+1$ that $\forall k: (\forall l: l<k \Rightarrow v_{il}=v_{(i+1)l}) \Rightarrow \forall v_{ik}\leq v_{(i+1)k}$ can be done by posting constraint \ref{shatter_constraint} for special variable permutations, which we will call \emph{subsequent $M$-row-swapping permutations}:
\begin{definition}
Given a CSP $\Pi$ that is row interchangeable for some matrix mapping $M$, then each variable permutation $R_{i,j}$ is an \emph{$M$-row-swapping permutation} if $R_{i,j}(v_{ik})=v_{jk}$, $R_{i,j}(v_{jk})=v_{ik}$, and if $R_{i,j}(v_{lk})=v_{lk}$ for $l \neq i,j$. An $M$-row-swapping permutation $R_{i,j}$ is \emph{subsequent} if $j=i+1$, here assuming a total ordering on the rows of $M$.
\end{definition}\bart{herschrijf definitie}\bart{ook: de piecewise is weer verdwenen. Je wilt niet al je resultaten beperken tot wholewise denk ik}
Posting constraint \ref{shatter_constraint} \bart{niet constraint \ref{shatter_constraint} maar constraint (\ref{shatter_constraint}). OOk: deze is te ver terug ik zou hem nog eens herhalen.}  for some subsequent $M$-row-swapping permutation $R_{i,i+1}$ and a total ordering on the variables where $v_{ij}<v_{kl} \Leftrightarrow i<k \lor (i=k \land j<l)$, then enforces that all variables in a row are lexicographically ordered according to the columns. This leads to the following corollary:
\begin{corollary}
\label{generators_break_completely}
Given a CSP $\Pi$ that is row interchangeable for some matrix mapping $M$, and given a total order on $M$'s rows, columns and values, then the conjunction of constraint \ref{shatter_constraint} for each subsequent $M$-row-swapping permutation of $\Pi$ using a fixed ordering of the variables where $v_{ij}<v_{kl} \Leftrightarrow i<k \lor (i=k \land j<l)$, is a complete symmetry breaking constraint for $\Pi$'s $M$-row interchangeability. \bart{dit suggereert dat dit maar 1 constraint is ipv heel veel instantiaties (cfr a polynomial number of constraints...}
\end{corollary}
If $n$ is the size of the rows of $M$, and $m$ the size of $M$'s columns, then per Corollary \ref{generators_break_completely} we will need $n-1$ symmetry breaking constraints, which each are $O(m)$ in size \cite{Shatter}. In the end, our symmetry breaking constraint is polynomial: $O(nm)$.

Corollary \ref{generators_break_completely} is very applicable to SAT-problems as well:
\begin{example}
\bart{niet gelezen... Ik vind die formules niet mooi (rechts aligned)... }
\label{ph_fixed}
Continuing Example \ref{small_ph}, it is clear that given the order $1{<_r}2{<_r}3$ on $M$'s rows, $R_{12}$ is a subsequent $M$-row-swapping permutation, while $R_{13}$ is not. We can solve this by changing the order on $M$'s rows to $2{<_r}1{<_r}3$. We should also adjust our variable ordering so that $w_{ij}<w_{kl} \Leftrightarrow i{<_r}k \lor (i{=_c}k \land j{<_c}l)$, for instance by using:
\begin{equation}
w_{21} < w_{22} < w_{11} < w_{12} < w_{31} < w_{32}
\end{equation}
The resulting symmetry breaking constraint would then become:
\begin{equation}
\label{symbreaking_constr}
\begin{array}{lr}
\text{permutation } R_{12}: & w_{21} \Rightarrow w_{11}\\
& (w_{21} \Leftrightarrow w_{11}) \Rightarrow (w_{22} \Rightarrow w_{12}) \\
\text{permutation } R_{13}: & w_{11} \Rightarrow w_{31} \\
& (w_{11} \Leftrightarrow w_{31}) \Rightarrow (w_{12} \Rightarrow w_{32}) \\
\text{permutation } C_{12}: & w_{11} \Rightarrow w_{12} \\
& (w_{11} \Leftrightarrow w_{12}) \Rightarrow (w_{21} \Rightarrow w_{22}) \\
& (w_{11} \Leftrightarrow w_{12}) \land (w_{21} \Leftrightarrow w_{22}) \Rightarrow (w_{31} \Rightarrow w_{32}) \\
\end{array}
\end{equation}
Note that now $\alpha_1$ does not satisfy $w_{21} \Rightarrow w_{11}$, and thus is removed from the solution space.\\
\end{example}
Adjusting the order on the rows is not the only way to obtain a complete symmetry breaking constraint via Corollary \ref{generators_break_completely} in Example \ref{ph_fixed}. We could have searched for the complete set of subsequent $M$-row-swapping generators by looking at compositions of $R_{12}$, $R_{13}$ and $C_{12}$. Indeed, if we take $R_{23}=R_{12} \circ R_{13} \circ R_{12}$, then \{$R_{12}; R_{23}\}$ is the set of all subsequent $M$-row-swapping permutations of $PH_{3,2}$, according to the original order on $M$'s rows.

\subsection{Detecting interchangeable rows in SAT}
In the previous section we discussed how, given a fairly obvious set of generators for row interchangeability in SAT, we could break the corresponding symmetry group efficiently. As opposed to detecting row interchangeability in CSPs \cite{mears_sym_detection,compositional_sym_detection}, detecting row interchangeability and corresponding matrix mappings in SAT is so far an uninvestigated topic.
\bart{``as opposed'' suggereert: in CP is dit al onderzocht. Maar basically zeggen we nadien. Maar van wat zij gedaan hebben trekken we ons niks aan: hier is een nieuwe, ad hoc, manier om het voor sat te doen.  Hoe werken die? Kunnne we hun technieken transferren? Waarom wel/niet? Wat is nodig om hun technieken te transferren? Wat hebben wij gedaan?}

The problem is also significantly harder in a SAT context. While a high level (CP) language shows much structure in the form of global constraints, quantifiers etc., a CNF only consists of clauses. These clauses can be shuffled and their literals randomly renamed, removing even more structural information. As a result, it is not easy to detect structured symmetries such as row interchangeability. \bart{maar SAT is ook CP.... Dit roept vragen op. Voorbeeldje geven?} \bart{ook zeggen: ze detecteren ALS de matrix gegeven is, is gemakkelijk... MAAR de matrix is meestal niet gegeven in SAT... TENZIJ dat ons SAT probleem komt van een higher order language (e.g. het is een CP encoding) Dan is het sim:=pel}

Currently, the approach we propose is a heuristic one. It is based on two intuitions:
\begin{enumerate}
\item $M$-row-swapping permutations are typically small compared to other symmetry inducing permutations. \bart{is dat zo? Waarom? Argumenteer: de volledige symmetriegroep waar we interesse in hebben bestaat uit row swaps in 1 grote matrix (denk ik): twee swaps samenstellen geeft een ``grotere'' (definieer groot) symmetrie}
\item sets of $M$-row-swapping permutations form relatively easy to recognize generators for $M$-row interchangeability. \bart{snap ik niet}
\end{enumerate}
\bart{ah: nu leg je uit. Ik zou eerst de uitleg geven en nadien nog eens samenvatten welke intuites je gebruikt (want nu was ik me vragen aan het stellen bij die enumeratie}
1. follows from the fact that given $n$ variables, a matrix mapping over these variables with $m$ rows can have at most $n/m$ \bart{at most???} variables for each row, and as a result, an $M$-row-swapping permutation permutes only $2*n/m$ variables \bart{ja ok. en waarom is dit klein?}.
2. follows from the fact that the permuted variables of an $M$-row-swapping permutation can be partitioned into two disjoint sets. Each of these sets forms a ``row'', which can be identical to other ``rows'' of an $M$-row-swapping permutation if both belong to the same $M$-row-swapping permutation group.\bart{snap ik nog steeds niet}

Combined with the fact that $M$-row-swapping permutations are their own inverse, we can heuristically search for small variable permutations, extract those that are their own inverse, and use a greedy union-find algorithm to find the biggest sets of $M$-row-swapping that together induce $M$-row interchangeability. The detected $M$-row interchangeability can then be broken efficiently using the technique described in the previous section. \bart{pfff}

%\jo{todo: work out a difficult symmetry breaking example extending the $PH_{3,2}$-examples.}

%It is well known that the permutation group of a set can be generated by the set of permutations that swap two subsequent elements:
%\begin{proposition}
%Given a CSP $\Pi$ that is row interchangeable for some matrix mapping $M$, and a total ordering on the rows of $M$, then the group of $M$-row permutations inducing $\Pi$'s row symmetry group is generated by the set of all subsequent $M$-row-swapping permutations.
%\end{proposition}
%This also means that given any generator set $G$ of the group of $M$-row permutations, we can compose elements of $G$ to obtain the set of all subsequent $M$-row-swapping permutations.

%Note that each $M$-row swapping permutation $R_{i,j}$ is its own inverse.
\end{comment}
