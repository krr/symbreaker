This repository contains BreakID 1.0, the first version of a new symmetry breaking preprocessor for SAT solvers, and cnfdedup, a tool to delete duplicate clauses in a cnf theory (needed in order to use Saucy).

**A new and strongly improved version of BreakID is available at [bitbucket.org/krr/breakid](https://bitbucket.org/krr/breakid). Please use that version instead of this one, which is no longer maintained and only kept online because of its mention in old papers.**

The folder "SAT13_solver_description" contains some more information why BreakID and cnfdedup are useful.

Copyright 2013 KU Leuven
Use of BreakID is governed by the GNU LGPLv3.0 license
Use of cnfdedup is totally free!
K.U.Leuven, Departement Computerwetenschappen,
Celestijnenlaan 200A, B-3001 Leuven, Belgium